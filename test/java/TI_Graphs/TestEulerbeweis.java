package TI_Graphs;

import Algorithms.Eulerbeweis;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by timo on 10.05.16.
 */
public class TestEulerbeweis {

    Graph testGraph;

    @Before
    public void setup() {
        testGraph = new SingleGraph("TestGraph");
        testGraph.addNode("A");
        testGraph.addNode("B");
        testGraph.addNode("C");
        testGraph.addNode("D");
        testGraph.addNode("E");
        testGraph.addEdge("AB", "A", "B");
        testGraph.addEdge("AC", "A", "C");
        testGraph.addEdge("AD", "A", "D");
        testGraph.addEdge("BC", "B", "C");
        testGraph.addEdge("BD", "B", "D");
        testGraph.addEdge("CD", "C", "D");
        testGraph.addEdge("CE", "C", "E");
        testGraph.addEdge("DE", "D", "E");

        testGraph.display(true);

    }

    @Test
    public void testEulerkreis(){
        assertFalse(Eulerbeweis.hasEulercircle(testGraph));
    }

    @Test
    public void testEulerpfad(){
        assertTrue(Eulerbeweis.hasEulerPath(testGraph));
    }

}
