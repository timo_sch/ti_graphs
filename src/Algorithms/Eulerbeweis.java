package Algorithms;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.Iterator;

/**
 * Created by timo on 09.05.16.
 */
public class Eulerbeweis {
    public static boolean hasEulercircle(Graph graph) {
        Iterable<? extends org.graphstream.graph.Node> list = graph.getEachNode();

        for (Node n : list) {
            if (n.getDegree() % 2 != 0)
                return false;
        }
        return true;

    }

    public static boolean hasEulerPath(Graph graph) {
        Iterable<? extends org.graphstream.graph.Node> list = graph.getEachNode();
        int end = 0;
        for (Node n : list) {
            if (end > 2)
                return false;
            if (n.getDegree() % 2 != 0)
                end++;
        }
        if (end == 0 || end == 2)
            return true;
        else
            return false;

    }
}
