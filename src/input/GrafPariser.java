package input;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;

import java.io.*;

/**
 * Created by alexander Hieser on 09.05.16.
 * Class for parsing the input data
 */
public class GrafPariser {

    private File graphFile;

    /**
     * Constructor
     *
     * @param graph
     */
    public GrafPariser(File graph) {
        this.graphFile = graph;
        try {
            readInputData();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Iterate through every line of text and prepare a new graph from it
     *
     * @return
     * @throws FileNotFoundException
     */
    public SingleGraph readInputData() throws FileNotFoundException {
        if (graphFile != null) {
            BufferedReader reader = new BufferedReader(new FileReader(graphFile));
            String line = "";
            SingleGraph graph = new SingleGraph(graphFile.getName());
            while (line != null) {
                try {
                    line = reader.readLine();
                    if (line != null) {
                        System.out.println(line);
                        prepareLine(graph, line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return graph;
        } else {
            return new SingleGraph("Error");
        }
    }

    /**
     * Choose if its vertex or edge
     *
     * @param graph
     * @param line
     */
    private void prepareLine(Graph graph, String line) {
        String[] array = line.split(" ");
        if (array != null) {
            switch (array[0]) {
                case "knoten": {
                    createVertex(graph, array);
                    break;
                }
                case "kante": {
                    createEdge(graph, array);
                }
                default:
                    return;
            }
        }
    }


    /**
     * Create Edge from the given string array
     *
     * @param graph
     * @param edge
     * @return
     */
    private Graph createEdge(Graph graph, String[] edge) {
        if (checkForOptional(edge)) {
            graph.addEdge(edge[1] + " - " + edge[2], edge[1], edge[2]).addAttribute(edge[3]);
        } else {
            graph.addEdge(edge[1] + " - " + edge[2], edge[1], edge[2]).addAttribute("0");
        }
        return graph;
    }

    /**
     * Create Vertex from the given string array
     *
     * @param graph
     * @param vertex
     * @return
     */
    private Graph createVertex(Graph graph, String[] vertex) {
        if (checkForOptional(vertex)) {
            graph.addNode(vertex[1]).addAttribute(vertex[2]);
        } else {
            graph.addNode(vertex[1]).addAttribute("0");
        }
        return graph;
    }

    private boolean checkForOptional(String[] line) {
        switch (line[0]) {
            case "knoten": {
                if (line.length > 2) {
                    if (line[2].isEmpty())
                        return false;
                    return true;
                } else {
                    return false;
                }
            }
            case "kante": {
                if (line.length > 3) {
                    if (line[3].isEmpty())
                        return false;
                    return true;
                } else {
                    return false;
                }
            }
            default:
                return false;
        }
    }

}
